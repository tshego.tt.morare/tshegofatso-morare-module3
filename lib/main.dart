import 'dart:html';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text(
            'Horizon.inc',
            style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
          ),
          leading: Icon(Icons.handshake_outlined, size: 30.0),
          centerTitle: true,
        ),
        body: const LoginPage(),
      ),
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  LoginPageState createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        padding: EdgeInsets.only(top: 30.0),
        child: Column(
          children: <Widget>[
            Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    child: Container(
                      //width: 500.0,
                      ///height: 300.0,
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: new Image(
                                width: 350.0,
                                height: 350.0,
                                fit: BoxFit.none,
                                image: new AssetImage('images/forgot.jpg')),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: 20.0,
                                bottom: 20.0,
                                left: 25.0,
                                right: 25.0),
                            child: TextFormField(
                                controller: email,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  icon: Icon(Icons.email_outlined),
                                  hintText: 'enter your email',
                                ),
                                //validator
                                validator: ((value) {
                                  if (value == null || value.isEmpty) {
                                    return 'please enter your email';
                                  } else if (value.length < 5 ||
                                      value.length > 40) {
                                    return 'email is invalid must be at least 5 chars but not more than 40 chars';
                                  } else if (!RegExp(r'\S+@\S+\.\S+')
                                      .hasMatch(value)) {
                                    return 'please enter valid email';
                                  } else {
                                    return null;
                                  }
                                })),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 20.0,
                              bottom: 20.0,
                              left: 25.0,
                              right: 25.0,
                            ),
                            child: TextFormField(
                                controller: password,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  icon: Icon(Icons.lock_outline),
                                  hintText: 'please enter password',
                                ),
                                obscureText: true,
                                //validator
                                validator: ((value) {
                                  if (value == null || value.isEmpty) {
                                    return 'please enter your password';
                                  } else if (value.length < 5) {
                                    return 'password is too short';
                                  } else if (value.length > 40) {
                                    return 'password is too long';
                                  } else {
                                    return null;
                                  }
                                })),
                          ),
                          Container(
                            child: Column(
                              children: <Widget>[
                                ElevatedButton.icon(
                                  onPressed: () {
                                    print("form submitted");
                                    if (_formKey.currentState!.validate()) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        const SnackBar(
                                            content: Text('processing data')),
                                      );
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => AccountPage(
                                                  email: email.text,
                                                  password: password.text,
                                                )),
                                      );
                                    } else {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        const SnackBar(
                                            content: Text(
                                                'Form could not be submitted')),
                                      );
                                    }
                                  },
                                  icon: const Icon(
                                    Icons.key_outlined,
                                    color: Colors.white,
                                    size: 20.0,
                                  ),
                                  label: const Text('Login',
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.white,
                                      )),
                                  style: ElevatedButton.styleFrom(
                                    fixedSize: const Size(500, 50),
                                    padding: EdgeInsets.all(20.0),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0)),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: EdgeInsets.all(20.0),
                              child: ElevatedButton.icon(
                                onPressed: () {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content: Text('opening sign up page')),
                                  );
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SignUpPage()),
                                  );
                                },
                                icon: const Icon(
                                  Icons.lock_outline,
                                  color: Colors.white,
                                  size: 20.0,
                                ),
                                label: const Text('SignUp',
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.white,
                                    )),
                                style: ElevatedButton.styleFrom(
                                  fixedSize: const Size(500, 50),
                                  padding: EdgeInsets.all(20.0),
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(25.0)),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ))
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class AccountPage extends StatelessWidget {
  String email, password;
  final _formKey = GlobalKey<ScaffoldState>();

  AccountPage({required this.email, required this.password});

  @override
  Widget build(BuildContext context) {
    TextEditingController email2 = TextEditingController(text: email);
    TextEditingController password2 = TextEditingController(text: password);

    return Scaffold(
      key: _formKey,
      appBar: AppBar(
        title: const Text(
          'Home',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
        leading: Icon(Icons.handshake_outlined, size: 30.0),
        centerTitle: true,
      ),
      body: Center(
        child: Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0))),
          child: InkWell(
            child: Column(
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8.0),
                        topRight: Radius.circular(8.0))),
                Image.asset('images/forgot.jpg',
                    width: 350, height: 350, fit: BoxFit.fill),
                ListTile(
                  leading: Icon(Icons.arrow_drop_down_circle_rounded),
                  title: const Text('Account'),
                  subtitle: Text(
                    email,
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'Store and retrieve values.',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: TextField(
                          controller: email2,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Email',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: TextField(
                          controller: password2,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Password',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => HomePage(
                      email: email,
                      password: password,
                    )),
          );
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('returning home')),
          );
        },
        label: const Text('Home'),
        icon: const Icon(Icons.house),
        backgroundColor: Colors.red,
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  String email, password;
  HomePage({required this.email, required this.password});

  @override
  Widget build(BuildContext context) {
    TextEditingController email = TextEditingController();
    TextEditingController password = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Home',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
        leading: Icon(Icons.handshake_outlined, size: 30.0),
        centerTitle: true,
      ),
      body: Center(
        child: Card(
          child: Column(
            children: <Widget>[
              const ListTile(
                leading: Icon(Icons.person),
                title: Text('Welcome to my app'),
                subtitle: Text('just use it'),
              ),
              Container(
                alignment: Alignment.center,
                child: Image.asset('images/person1.png'),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => AccountPage(
                      email: email.text,
                      password: password.text,
                    )),
          );
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Account edit')),
          );
        },
        label: const Text('Account'),
        icon: const Icon(Icons.person),
        backgroundColor: Colors.red,
      ),
    );
  }
}

class SignUpPage extends StatefulWidget {
  @override
  SignUpState createState() {
    return SignUpState();
  }
}

class SignUpState extends State<SignUpPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        padding: EdgeInsets.only(top: 30.0),
        child: Column(
          children: <Widget>[
            Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    child: Container(
                      //width: 500.0,
                      ///height: 300.0,
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: new Image(
                                width: 350.0,
                                height: 350.0,
                                fit: BoxFit.none,
                                image: new AssetImage('images/forgot.jpg')),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: 20.0,
                                bottom: 20.0,
                                left: 25.0,
                                right: 25.0),
                            child: TextFormField(
                                controller: email,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  icon: Icon(Icons.email_outlined),
                                  hintText: 'enter your email',
                                ),
                                //validator
                                validator: ((value) {
                                  if (value == null || value.isEmpty) {
                                    return 'please enter your email';
                                  } else if (value.length < 5 ||
                                      value.length > 40) {
                                    return 'email is invalid must be at least 5 chars but not more than 40 chars';
                                  } else if (!RegExp(r'\S+@\S+\.\S+')
                                      .hasMatch(value)) {
                                    return 'please enter valid email';
                                  } else {
                                    return null;
                                  }
                                })),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 20.0,
                              bottom: 20.0,
                              left: 25.0,
                              right: 25.0,
                            ),
                            child: TextFormField(
                                controller: password,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  icon: Icon(Icons.lock_outline),
                                  hintText: 'please enter password',
                                ),
                                obscureText: true,
                                //validator
                                validator: ((value) {
                                  if (value == null || value.isEmpty) {
                                    return 'please enter your password';
                                  } else if (value.length < 5) {
                                    return 'password is too short';
                                  } else if (value.length > 40) {
                                    return 'password is too long';
                                  } else {
                                    return null;
                                  }
                                })),
                          ),
                          Container(
                            child: Column(
                              children: <Widget>[
                                ElevatedButton.icon(
                                  onPressed: () {
                                    print("form submitted");
                                    if (_formKey.currentState!.validate()) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        const SnackBar(
                                            content: Text('processing data')),
                                      );
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => AccountPage(
                                                  email: email.text,
                                                  password: password.text,
                                                )),
                                      );
                                    } else {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        const SnackBar(
                                            content: Text(
                                                'Form could not be submitted')),
                                      );
                                    }
                                  },
                                  icon: const Icon(
                                    Icons.lock_outline,
                                    color: Colors.white,
                                    size: 20.0,
                                  ),
                                  label: const Text('Sign Up',
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.white,
                                      )),
                                  style: ElevatedButton.styleFrom(
                                    fixedSize: const Size(500, 50),
                                    padding: EdgeInsets.all(20.0),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0)),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: EdgeInsets.all(20.0),
                              child: ElevatedButton.icon(
                                onPressed: () {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content: Text('opening login page')),
                                  );
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginPage()),
                                  );
                                },
                                icon: const Icon(
                                  Icons.key_outlined,
                                  color: Colors.white,
                                  size: 20.0,
                                ),
                                label: const Text('Login',
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.white,
                                    )),
                                style: ElevatedButton.styleFrom(
                                  fixedSize: const Size(500, 50),
                                  padding: EdgeInsets.all(20.0),
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(25.0)),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ))
              ],
            ),
          ],
        ),
      ),
    );
  }
}
